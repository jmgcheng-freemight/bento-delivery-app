### What is this repository for? ###

* This is a Wordpress Plugin. Used and Called at Delivery Base App
* Delivery Base App is a mobile app
* This plugin will setup a Wordpress so it can receive data coming from a mobile app and save it to a custom post type bda-order

### How do I get set up? ###

* Activate in Wordpress Plugin
* Create a page with a slug 'bda-order-page-app'. Slug depends on what the mobile app will call