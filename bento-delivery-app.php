<?php
/*
Plugin Name: Bento Delivery App
Description: Bento Delivery App
Version:     0.1
Author:      Jan Michael Cheng
*/
define( 'BDA_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'BDA_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );


/*
	plugin activation and deactivation
*/
function bda_activate()
{
	//delete to clear first
	bda_delete_app_pages();
	//then create
	bda_create_app_pages();
}
register_activation_hook( __FILE__, 'bda_activate' );

function bda_deactivate()
{
	bda_delete_app_pages();
}
register_deactivation_hook( __FILE__, 'bda_deactivate' );



/*
	create pages to be connected to our plugin templates
*/
function bda_create_app_pages() {

	$b_result = true;
	
	/*
	*/
	$a_args = array(
		'post_title'    => 'BDA Order Page App',
		'post_status'   => 'publish',
		'post_type' => 'page'
	);
	$i_insert_result = wp_insert_post( $a_args );
	if( is_int($i_insert_result) && $i_insert_result > 0 )
	{ }
	else
	{ $b_result = false; }

	/*
	*/
	$a_args = array(
		'post_title'    => 'BDA Menu Page App',
		'post_status'   => 'publish',
		'post_type' => 'page'
	);
	$i_insert_result = wp_insert_post( $a_args );
	if( is_int($i_insert_result) && $i_insert_result > 0 )
	{ }
	else
	{ $b_result = false; }

}
function bda_delete_app_pages() {
	
	/*
		delete bda-order-page-app
	*/
	$o_query_result = new WP_Query( array( 'pagename' => 'bda-order-page-app' ) );
	while( $o_query_result->have_posts() ) 
	{
		$o_query_result->the_post();
		wp_delete_post(get_the_ID(), true);
	}
	
	/*
		delete bda-menu-page-app
	*/
	$o_query_result = new WP_Query( array( 'pagename' => 'bda-menu-page-app' ) );
	while( $o_query_result->have_posts() ) 
	{
		$o_query_result->the_post();
		wp_delete_post(get_the_ID(), true);
	}
}


/*
	use templates inside plugin folder if slugs are these
*/
function bda_templates( $s_template ) {

	if( is_page( 'bda-order-page-app' ) )
	{
		$s_template_temp_dir = BDA_PLUGIN_DIR . 'bda-order-page-app.php';
		return $s_template_temp_dir ;
	}
	elseif( is_page( 'bda-menu-page-app' ) )
	{
		$s_template_temp_dir = BDA_PLUGIN_DIR . 'bda-menu-page-app.php';
		return $s_template_temp_dir ;
	}
	return $s_template;
}
add_filter( 'template_include', 'bda_templates', 99 );




/*
*/
function bda_init() 
{
	/*
	*/
	add_theme_support( 'post-thumbnails', array( 'bda-menu' ) ); 
	add_image_size( '600x600nocrop', 600, 600 );
	
	
	/*
		bda-order Post Type
	*/
	$a_labels = array(
		'name'                => _x( 'BDA Orders', 'Post Type General Name' ),
		'singular_name'       => _x( 'BDA Order', 'Post Type Singular Name' ),
		'menu_name'           => __( 'BDA Orders' ),
		'parent_item_colon'   => __( 'Parent Order' ),
		'all_items'           => __( 'All Orders' ),
		'view_item'           => __( 'View Order' ),
		'add_new_item'        => __( 'Add New Order' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Order' ),
		'update_item'         => __( 'Update Order' ),
		'search_items'        => __( 'Search Order' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$a_args = array(
		'label'               => __( 'BDA orders' ),
		'description'         => __( 'BDA Orders' ),
		'labels'              => $a_labels,
		'supports'            => array( 'title' ), /* , 'editor', 'custom-fields' */
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true, //true
		'show_in_menu'        => true, //true
		'show_in_nav_menus'   => true, //true
		'show_in_admin_bar'   => true, //true
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true
	);
	register_post_type( 'bda-order', $a_args );
	
	
	/*
		bda-menu Post Type
	*/
	$a_labels = array(
		'name'                => _x( 'BDA Menus', 'Post Type General Name' ),
		'singular_name'       => _x( 'BDA Menu', 'Post Type Singular Name' ),
		'menu_name'           => __( 'BDA Menus' ),
		'parent_item_colon'   => __( 'Parent Menu' ),
		'all_items'           => __( 'All Menus' ),
		'view_item'           => __( 'View Menu' ),
		'add_new_item'        => __( 'Add New Menu' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Menu' ),
		'update_item'         => __( 'Update Menu' ),
		'search_items'        => __( 'Search Menu' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
	$a_args = array(
		'label'               => __( 'BDA Menus' ),
		'description'         => __( 'BDA Menus' ),
		'labels'              => $a_labels,
		'supports'            => array( 'title', 'thumbnail', 'editor' ), /* , 'editor', 'custom-fields' */
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true, //true
		'show_in_menu'        => true, //true
		'show_in_nav_menus'   => true, //true
		'show_in_admin_bar'   => true, //true
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true
	);
	register_post_type( 'bda-menu', $a_args );
	
	
	
}
add_action( 'init', 'bda_init' );


/*
	BDA Meta Box
*/
function bda_custom_meta_box()
{
	add_meta_box("bda_order_details_meta_box", "Order Details", "bda_order_details_view", "bda-order", "normal", "high", null);
	add_meta_box("bda_menu_details_meta_box", "Details", "bda_menu_details_view", "bda-menu", "side", "default", null);
}
add_action("add_meta_boxes", "bda_custom_meta_box");
//
function bda_order_details_view($o_post)
{
	wp_nonce_field(basename(__FILE__), "bda_order_details_meta_box_nonce");
	$o_order_meta = json_decode(get_post_meta($o_post->ID, "bda_order_details", true));
	$d_grand_total = 0;
	$d_expected_change = 0;
	?>
		
		<div class="bda-order-details">
			<div class="bda-order-details-customer-detail">
				<div>
					<div class="bda-order-details-customer-detail-label">
						<label for="bda_fullname">Customer Name:</label>
					</div>
					<div class="bda-order-details-customer-detail-info">
						<input name="bda_fullname" type="text" value="<?php echo $o_order_meta->o_customer->s_fullName; ?>" readonly="readonly" />
					</div>
				</div>
				<div>
					<div class="bda-order-details-customer-detail-label">
						<label for="bda_email">Customer Email:</label>
					</div>
					<div class="bda-order-details-customer-detail-info">
						<input name="bda_email" type="text" value="<?php echo $o_order_meta->o_customer->s_email; ?>" readonly="readonly" />
					</div>
				</div>
				<div>
					<div class="bda-order-details-customer-detail-label">
						<label for="bda_cellphonenumber">Customer Contact Number:</label>
					</div>
					<div class="bda-order-details-customer-detail-info">
						<input name="bda_cellphonenumber" type="text" value="<?php echo $o_order_meta->o_customer->s_cellphoneNumber; ?>" readonly="readonly" />
					</div>
				</div>
				<div>
					<div class="bda-order-details-customer-detail-label">
						<label for="bda_cashOnHand">Customer Cash on Hand:</label>
					</div>
					<div class="bda-order-details-customer-detail-info">
						<input name="bda_cashOnHand" type="text" value="<?php echo number_format($o_order_meta->o_customer->d_cashOnHand, 2); ?>" readonly="readonly" />
					</div>
				</div>
				<div>
					<div class="bda-order-details-customer-detail-label">
						<label for="bda_orderNote">Customer Order Note:</label>
					</div>
					<div class="bda-order-details-customer-detail-info">
						<textarea name="bda_orderNote" readonly="readonly" ><?php echo $o_order_meta->o_customer->s_orderNote; ?></textarea>
					</div>
				</div>
				<div>
					<div class="bda-order-details-customer-detail-label">
						<label for="bda_deliveryAddress">Customer Address:</label>
					</div>
					<div class="bda-order-details-customer-detail-info">
						<textarea name="bda_deliveryAddress" readonly="readonly" ><?php echo $o_order_meta->o_customer->s_deliveryAddress; ?></textarea>
					</div>
				</div>
				<div>
					<div class="bda-order-details-customer-detail-label">
						<label for="bda_latLong">Google Map:</label>
					</div>
					<div class="bda-order-details-customer-detail-info">
						<input name="bda_latLong" type="text" value="<?php echo $o_order_meta->o_customer->s_gpsLat . ':' . $o_order_meta->o_customer->s_gpsLong; ?>" readonly="readonly" hidden="hidden" />
						<input name="bda_lat" type="text" value="<?php echo $o_order_meta->o_customer->s_gpsLat; ?>" readonly="readonly" hidden="hidden" />
						<input name="bda_long" type="text" value="<?php echo $o_order_meta->o_customer->s_gpsLong; ?>" readonly="readonly" hidden="hidden" />
						<div id="js-bda-order-details-customer-detail-info-gmap"></div>
					</div>
				</div>
				
			</div>
			
			<?php
				$a_cart = (array)$o_order_meta->o_cart;
				if( isset($a_cart) && !empty($a_cart) ) :
			?>
			<table class="bda-order-details-cart-detail">
				<thead>
					<tr>
						<th>
							Menu Name
						</th>
						<th>
							Cost
						</th>
						<th>
							Quantity
						</th>
						<th>
							Sub Total
						</th>
					</tr>
				</thead>
				</tbody>
					<?php
						foreach( $o_order_meta->o_cart AS $o_order ):
					?>
					<tr>
						<td>
							<?php echo $o_order->o_menuDetails->s_name; ?>
						</td>
						<td>
							<?php echo number_format( $o_order->o_menuDetails->d_price, 2 ); ?>
						</td>
						<td>
							<?php echo $o_order->i_qty; ?>
						</td>
						<td>
							<?php echo number_format( $o_order->d_subTotal, 2 ); ?>
						</td>
					</tr>
					<?php
						$d_grand_total = $d_grand_total + $o_order->d_subTotal;
						endforeach;
					?>
				</tbody>
			</table>
			<?php
				else:
			?>
			<table class="bda-order-details-cart-detail">
				<tbody>
					<tr>
						<th>
							No Cart Detail Recorded
						</th>
					</tr>
				</tbody>
			</table>
			<?php
				endif;
			?>
			
			<div class="bda-order-details-summary">
				<div>
					<div class="bda-order-details-summary-label">
						<label for="bda_grandTotal">Grand Total:</label>
					</div>
					<div class="bda-order-details-summary-info">
						<input name="bda_grandTotal" type="text" value="<?php echo number_format($d_grand_total, 2); ?>" readonly="readonly" />
					</div>
				</div>
				<div>
					<div class="bda-order-details-summary-label">
						<label for="bda_expectedChange">Customer Expected Change:</label>
					</div>
					<div class="bda-order-details-summary-info">
						<input name="bda_expectedChange" type="text" value="<?php echo number_format($o_order_meta->o_customer->d_cashOnHand - $d_grand_total, 2); ?>" readonly="readonly" />
					</div>
				</div>
			</div>
			
		</div>
		
	<?php
}
//
function bda_menu_details_view($o_post)
{
	wp_nonce_field(basename(__FILE__), "bda_menu_details_meta_box_nonce");
	?>
	
		<div class="bda-menu-details">
			<div>
				<div class="bda-menu-details-label">
					<label for="bda_menuCost">Cost:</label>
				</div>
				<div class="bda-menu-details-info">
					<input name="bda_menuCost" type="text" value="<?php echo get_post_meta($o_post->ID, "bda_menuCost", true); ?>" placeholder="0.00" />
				</div>
			</div>
		</div>
	
	<?php
}


/*
	BDA Admin Panel
*/
add_action( 'admin_menu', 'bda_plugin_panel' );

function bda_plugin_panel() {
	add_options_page( 'BDA Admin Panel', 'BDA Admin Panel', 'manage_options', 'bda-admin-panel', 'bda_admin_panel' );
}
//
function bda_admin_panel() {
	
		if( isset($_POST) && !empty($_POST) )
		{

			if( isset($_POST['txt_bda_nonce']) && !empty($_POST['txt_bda_nonce']) )
			{
				if (!wp_verify_nonce($_POST['txt_bda_nonce'], 'bda_nonce'))
				{
					echo "<div id=\"message\" class=\"updated fade\"><p>Security Check - If you receive this in error, log out and back in to WordPress</p></div>";
					die();
				}
			}
			
			$s_title = '';
			$s_message = '';
			$b_complete_details = true;
			$b_has_warnings = false;
			$a_success = array();
			$a_warnings = array();
			$a_errors = array();
			
			if( isset($_POST['txt_bda_notification_title']) && !empty($_POST['txt_bda_notification_title']) )
			{ $s_title = $_POST['txt_bda_notification_title']; }
			else
			{
				array_push($a_warnings, "Title Required");
			}
			
			if( isset($_POST['txt_bda_notification_message']) && !empty($_POST['txt_bda_notification_message']) )
			{ $s_message = $_POST['txt_bda_notification_message']; }
			else
			{ 
				array_push($a_warnings, "Description Required");
			}
		
			/*
				try curl. Send to https://api.ionic.io/push/notifications
			*/
			if( empty($a_warnings) && empty($a_errors) )
			{
				$o_curl = curl_init();

				curl_setopt_array($o_curl, array(
					CURLOPT_URL => "https://api.ionic.io/push/notifications",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => "{
						\"send_to_all\":true,
						\"profile\":\"notifydev\",
						\"notification\":{
							\"title\":\"". $s_title ."\",
							\"message\":\"". $s_message ."\"
						}
					}",
					CURLOPT_HTTPHEADER => array(
						"authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI1OTk1MzliMS1lMjFlLTRlMjctYjI5OS1kZWRlOTg0ODg5YjIifQ.0sQYYPTk9cJS3kO5oeA6lXKhi5CozNCgldeU6bc75iw",
						"cache-control: no-cache",
						"content-type: application/json"
					)
				));

				$o_curl_response = json_decode(curl_exec($o_curl));
				$s_curl_err = curl_error($o_curl);

				curl_close($o_curl);

				if($s_curl_err) 
				{
					array_push($a_errors, "CURL Error - " . $s_curl_err);
				} 
				else 
				{
					if( isset($o_curl_response->meta->status) && !empty($o_curl_response->meta->status) && $o_curl_response->meta->status == 201 )
					{
						array_push($a_success, "Notification Sent");
					}
					if( isset($o_curl_response->error->message) && !empty($o_curl_response->error->message) )
					{
						array_push($a_warnings, $o_curl_response->error->message);
					}
				}
			}
		}
	?>

	<div class="" >
		<div class="bda-flash">
			<?php 
				if( isset($a_success) && !empty($a_success) ):
			?>
			<div class="bda-flash-message bda-flash-message-success">
				<?php 
					foreach( $a_success AS $s_success_msg ):
				?>
				<p><span>Success:</span> <?php echo $s_success_msg; ?></p>
				<?php 
					endforeach;
				?>
			</div>
			<?php 
				endif;
			?>
			<?php 
				if( isset($a_warnings) && !empty($a_warnings) ):
			?>
			<div class="bda-flash-message bda-flash-message-warning">
				<?php 
					foreach( $a_warnings AS $s_warning_msg ):
				?>
				<p><span>Warning:</span> <?php echo $s_warning_msg; ?></p>
				<?php 
					endforeach;
				?>
			</div>
			<?php 
				endif;
			?>

			<?php 
				if( isset($a_errors) && !empty($a_errors) ):
			?>
			<div class="bda-flash-message bda-flash-message-error">
				<?php 
					foreach( $a_errors AS $s_error_msg ):
				?>
				<p><span>Error:</span> <?php echo $s_error_msg; ?></p>
				<?php 
					endforeach;
				?>
			</div>
			<?php 
				endif;
			?>

			<!-- <div class="bda-flash-message bda-flash-message-error">
				<p><span>Error:</span> Error </p>
			</div> -->
		</div>
		<h1>
			Create Push Notification
		</h1>
		<p>
			Users who already logged-in using the app will received the notifications.
		</p>
		<form name="frm_create_bda_notification" id="frm_create_bda_notification" action="" method="post">
			<input type="hidden" name="txt_bda_nonce" value="<?php echo wp_create_nonce('bda_nonce'); ?>" />
			<div class="bda-form-details">
				<div class="bda-form-details-item">
					<div class="bda-form-details-item-field">
						<label>Title:</label>
					</div>
					<div class="bda-form-details-item-info">
						<input type="text" name="txt_bda_notification_title" value="" maxlength="20" />
					</div>
				</div>
				<div class="bda-form-details-item">
					<div class="bda-form-details-item-field">
						<label>Message:</label>
					</div>
					<div class="bda-form-details-item-info">
						<input type="text" name="txt_bda_notification_message" value="" maxlength="40" />
					</div>
				</div>
				<div class="bda-form-details-item">
					<div class="bda-form-details-item-control">
						<input type="submit" value="Send" class="button button-primary button-large" />
					</div>
				</div>
			</div>
		</form>
		
		<hr/>
		
	</div>
	
	<?php
}



/*
	Save Custom Meta Boxes Data
*/
function bda_save_menu_details_meta_box( $post_id ) {
	// Verify the nonce before proceeding.
	if ( !isset( $_POST['bda_menu_details_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['bda_menu_details_meta_box_nonce'], basename( __FILE__ ) ) )
	{
		return $post_id;
	}
	//
	if( !wp_is_post_revision( $post_id ) ) 
	{
		//reset first
		update_post_meta($post_id, 'bda_menuCost', '0.00');

		if( isset($_POST['bda_menuCost']) && !empty($_POST['bda_menuCost']) && is_numeric($_POST['bda_menuCost']) )
		{
			update_post_meta($post_id, 'bda_menuCost', $_POST['bda_menuCost']);
		}
	}
}
add_action( 'save_post', 'bda_save_menu_details_meta_box' );



/*
	CSS/JS - Admin - bda-order Post Type
*/
function bda_script_post_editor() {
	global $post_type;
	
	if( 
		( isset($post_type) && !empty($post_type) && ( $post_type == 'bda-order' || $post_type == 'bda-menu' )  )
		|| 
		( isset($_GET['page']) && !empty($_GET['page']) && $_GET['page'] == 'bda-admin-panel' )
	)
	{
		wp_enqueue_style( 'bda_css_admin', plugins_url('/css/style.css', __FILE__), array(), '1.0' );
		wp_enqueue_script('google-maps', 'http://maps.googleapis.com/maps/api/js');
		//wp_enqueue_script('google-angularjs', plugins_url('/js/angular.js', __FILE__));
		//wp_enqueue_script( 'bda_js_admin', plugins_url('/js/script.js', __FILE__), array('jquery'), '1.0' );
		wp_register_script( 
			'bda_js_admin', 
			plugins_url('/js/script.js', __FILE__), 
			array( 'jquery' )
		);
		wp_enqueue_script( 'bda_js_admin' );
		
	}
	
}
add_action( 'admin_enqueue_scripts', 'bda_script_post_editor' );


/*
	Remove - Admin Menus - Not really needed
function bda_remove_menus() {
	remove_menu_page( 'index.php' );                  
	remove_menu_page( 'edit.php' );                   
	remove_menu_page( 'upload.php' );                 
	remove_menu_page( 'edit.php?post_type=page' );    
	remove_menu_page( 'edit-comments.php' );          
	remove_menu_page( 'themes.php' );                 
	remove_menu_page( 'plugins.php' );                
	remove_menu_page( 'users.php' );                  
	remove_menu_page( 'tools.php' );                  
	remove_menu_page( 'options-general.php' );        
}
add_action( 'admin_menu', 'bda_remove_menus' );
*/






