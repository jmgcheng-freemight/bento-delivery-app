<?php
	header('Content-Type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	header('Access-Control-Allow-Headers: origin, content-type, accept');

	define("S_SITE_ORDER_APP_API", '1010deliverybaseapp1010');
	
	$a_result = array();
	$a_result['_post'] = $_POST;
	$a_result['s_error'] = '';
	$a_result['s_message'] = '';
	$a_result['b_success'] = false;
	
	
	/**/
	if( empty($_POST) )
	{
		wp_redirect( home_url() ); 
		exit();
	}
	
	
	/*
		check api key
	*/
	if( isset($_POST['s_site_order_app_api']) && !empty($_POST['s_site_order_app_api']) && $_POST['s_site_order_app_api'] == S_SITE_ORDER_APP_API )
	{ }
	else
	{
		$a_result['s_error'] = 'Invalid Key';
		echo json_encode($a_result);
		exit();
	}
	
	
	/*
		try insert wp_post
	*/
	$i_insert_result = '';
	$a_post_args = array();
	$a_post_args['s_timeStamp'] 		= ( isset($_POST['s_timeStamp']) && !empty($_POST['s_timeStamp']) ? $_POST['s_timeStamp'] : '' );
	$a_post_args['s_cellphoneNumber']	= ( isset($_POST['s_cellphoneNumber']) && !empty($_POST['s_cellphoneNumber']) ? $_POST['s_cellphoneNumber'] : '' );
	$a_post_args['s_deliveryAddress']	= ( isset($_POST['s_deliveryAddress']) && !empty($_POST['s_deliveryAddress']) ? $_POST['s_deliveryAddress'] : '' );
	$a_post_args['s_orderNote']			= ( isset($_POST['s_orderNote']) && !empty($_POST['s_orderNote']) ? $_POST['s_orderNote'] : '' );
	$a_post_args['o_customer']			= ( isset($_POST['o_customer']) && !empty($_POST['o_customer']) ? $_POST['o_customer'] : '' );
	$a_post_args['o_order']				= ( isset($_POST['o_order']) && !empty($_POST['o_order']) ? $_POST['o_order'] : '' );
	/**/
	$s_fullName 						= ( isset($a_post_args['o_customer']['s_fullName']) && !empty($a_post_args['o_customer']['s_fullName']) ? $a_post_args['o_customer']['s_fullName'] : '' );
	$s_cellphoneNumber 					= ( isset($a_post_args['o_customer']['s_cellphoneNumber']) && !empty($a_post_args['o_customer']['s_cellphoneNumber']) ? $a_post_args['o_customer']['s_cellphoneNumber'] : '' );
	/*
	*/
	$a_args = array(
		'post_title'    => 'App Order - '. $s_fullName .' - '. $s_cellphoneNumber . ' - ' . 'TS:' . $a_post_args['s_timeStamp'],
		'post_content'  => json_encode($_POST, JSON_UNESCAPED_UNICODE),
		'post_status'   => 'publish',
		'post_type' => 'bda-order'
		//'post_author'   => 1
	);
	$i_insert_result = wp_insert_post( $a_args );
	//
	if( is_int($i_insert_result) && $i_insert_result > 0 )
	{
		//insert meta data
		update_post_meta( $i_insert_result, 'bda_order_details', json_encode($_POST, JSON_UNESCAPED_UNICODE) );
		//
		$a_result['s_message'] = 'Order Posted';
		$a_result['b_success'] = true;
		echo json_encode($a_result);
		exit();
	}
	else
	{
		$a_result['s_error'] = 'Order NOT Posted';
		echo json_encode($a_result);
		exit();
	}
?>