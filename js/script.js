jQuery(document).ready(function(){
	
	/*
	*/
	var o_map_div = document.getElementById("js-bda-order-details-customer-detail-info-gmap");
	var s_bda_lat = ( jQuery("[name='bda_lat']").val() != undefined ) ? jQuery("[name='bda_lat']").val() : '';
	var s_bda_long = ( jQuery("[name='bda_long']").val() != undefined ) ? jQuery("[name='bda_long']").val() : '';
	var s_bda_deliveryAddress = ( jQuery("[name='bda_deliveryAddress']").val() != undefined ) ? jQuery("[name='bda_deliveryAddress']").val() : '';
	//console.log(s_bda_lat);
	//console.log(s_bda_long);
	//console.log(s_bda_deliveryAddress);
	
	
	
	function gmap_bda() 
	{
		/* base on Lat and Long
		*/
		var o_bda_cust_prop = {
			center:new google.maps.LatLng(s_bda_lat,s_bda_long),
			//center:new google.maps.LatLng(s_bda_lat,''),
			zoom:16,
			mapTypeId:google.maps.MapTypeId.ROADMAP
		};
		var o_map_bda_cust = new google.maps.Map(o_map_div,o_bda_cust_prop);
		/*
		var o_bda_cust_marker = new google.maps.Marker({ position:o_bda_cust_prop.center });
		o_bda_cust_marker.setMap(o_map_bda_cust);
		*/
		
		
		/* base on address
		*/
		var geocoder = new google.maps.Geocoder();
		//console.log(geocoder);
		if (geocoder) 
		{
			geocoder.geocode({
				'address': s_bda_deliveryAddress
				}, 
				function(results, status) 
				{
					if (status == google.maps.GeocoderStatus.OK) 
					{
						if (status != google.maps.GeocoderStatus.ZERO_RESULTS) 
						{
							o_map_bda_cust.setCenter(results[0].geometry.location);
							var marker = new google.maps.Marker({
								position: results[0].geometry.location,
								map: o_map_bda_cust
							}); 
						} 
						else 
						{
							jQuery(o_map_div).html('<p>Not available</p>');
							alert("No results found");
						}
					} 
					else 
					{
						jQuery(o_map_div).html('<p>Not available</p>');
						alert("Geocode was not successful for the following reason: " + status);
					}
				}
			);
		}
		else
		{
			jQuery(o_map_div).html('<p>Not available</p>');
			alert('Geocoder not loaded.');
		}
	}
	google.maps.event.addDomListener(window, 'load', gmap_bda);
	
	
	/*
	*/
	
	
});