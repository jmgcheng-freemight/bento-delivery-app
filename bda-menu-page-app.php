<?php
	header('Content-Type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	header('Access-Control-Allow-Headers: origin, content-type, accept');
	
	define("S_SITE_ORDER_APP_API", '1010deliverybaseapp1010');
	
	$a_result = array();
	$a_result['_post'] = $_POST;
	$a_result['s_error'] = '';
	$a_result['s_message'] = '';
	$a_result['a_menu'] = array();
	$a_result['b_success'] = false;
	$a_menu = array();

	
	/**/
	if( empty($_POST) )
	{
		wp_redirect( home_url() ); 
		exit();
	}
	
	
	/*
		check api key
	*/
	if( isset($_POST['s_site_order_app_api']) && !empty($_POST['s_site_order_app_api']) && $_POST['s_site_order_app_api'] == S_SITE_ORDER_APP_API )
	{ }
	else
	{
		$a_result['s_error'] = 'Invalid Key';
		echo json_encode($a_result);
		exit();
	}
	
	
	/*
		try get bda-menu posts
	*/
	$a_args = array(
		'post_type' => 'bda-menu',
		'post_status' => 'publish'
	);
	$o_bda_menu_result = new WP_Query( $a_args );
	if ( $o_bda_menu_result->have_posts() ) 
	{
		while( $o_bda_menu_result->have_posts() ) 
		{
			$o_bda_menu_result->the_post();
			$o_menu_template = (object) array();
			
			$o_menu_template->i_id 				= get_the_ID();
			$o_menu_template->s_name 			= get_the_title();
			$o_menu_template->s_description 	= get_the_content();
			$o_menu_template->s_image 			= '';
			$o_menu_template->d_price 			= get_post_meta(get_the_ID(), "bda_menuCost", true);
			if( has_post_thumbnail() ) 
			{
				$a_image_detail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), '600x600nocrop' );
				$o_menu_template->s_image = $a_image_detail[0];
			}
			//print_r($o_menu_template);
			array_push($a_menu, $o_menu_template);
		}
		$a_result['a_menu'] = $a_menu;
		$a_result['b_success'] = true;
		
		echo json_encode($a_result);
		exit();
	}
	else 
	{
		$a_result['s_message'] = 'No Menu';
		$a_result['b_success'] = true;
		
		echo json_encode($a_result);
		exit();
	}
?>